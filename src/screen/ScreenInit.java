/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screen;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

/**
 *
 * @author wookie
 */
public class ScreenInit {
    private static ScreenInit instance;
//    public static final int MAX_WIDTH = 1366;
//    public static final int MAX_HEIGHT = 768;
    private int maxWidth;
    private int naxHeight;
    private int width;
    private int height;
    private int offsetX;
    private int offsetY;
    
    private float compressionRate;

    private ScreenInit() {
        //graphicConfigInit();
    }
    
    public final void graphicConfigInit() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
       
        GraphicsDevice primaryScreen = gs[0];
        GraphicsConfiguration configuration = primaryScreen.getConfigurations()[0];
        offsetX = (int)(configuration.getBounds().getX());
        offsetY = (int)(configuration.getBounds().getY());   
        width = (int)(configuration.getBounds().getWidth());
        height = (int)(configuration.getBounds().getHeight());
        
        if(width > maxWidth)
            width = maxWidth;
        if(height > naxHeight)
            height = naxHeight;
        //for(GraphicsDevice curGs : gs)
//{
//      GraphicsConfiguration[] gc = curGs.getConfigurations();
//      for(GraphicsConfiguration curGc : gc)
//      {
//            Rectangle bounds = curGc.getBounds();
//
//            System.out.println(bounds.getX() + "," + bounds.getY() + " " + bounds.getWidth() + "x" + bounds.getHeight());
//      }
// }
//
//        System.out.println("screens::::::::::");
//for(GraphicsDevice curGs : gs)
//{
//      DisplayMode dm = curGs.getDisplayMode();
//      System.out.println(dm.getWidth() + " x " + dm.getHeight());
//}
    }

    public static ScreenInit getInstance() {
        if(instance == null) {
            instance = new ScreenInit();
        }
        
        return instance;
    }

    public float getCompressionRate() {
        return compressionRate;
    }

    public void setCompressionRate(float compressionRate) {
        this.compressionRate = compressionRate;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public void setNaxHeight(int naxHeight) {
        this.naxHeight = naxHeight;
    }
    
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }
    
    
}
