/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screen;

import java.io.File;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Rectangle;
import java.io.IOException;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.IIOImage;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import javax.swing.filechooser.FileSystemView;

/**
 * 
 * @author wookie
 */
public class ScreenCapture {  //Should be singletone
    private ScreenInit config = ScreenInit.getInstance();
    
    public ScreenCapture() {
        
    }
    

    
//    public static void main(String[] args) {
//        RenderedImage ri = null;
//        long start;
//        long end;
//        long traceTime;
//        graphicConfigInit();
//              
//        
//        for(int i = 0; i < 10; i++)
//		try {
//                        start = System.nanoTime();
//                        ri = grabScreen();
//                        end = System.nanoTime();
//                        traceTime = end-start;
//                        System.out.println("Time1: " + traceTime);
//                        
//                        start = System.nanoTime();
//			ImageIO.write(ri, "png", new File("screen" + i + ".png"));
//                        end = System.nanoTime();
//                        traceTime = end-start;
//                        System.out.println("Time2: " + traceTime);
//		} catch (IOException e) {
//			System.out.println("IO exception"+e);
//		}
//        
//        start = System.nanoTime();
//        convertRenderedImage(ri);
//        end = System.nanoTime();
//        traceTime = end-start;
//        System.out.println("Time3: " + traceTime);
//        
//	}
    
    public byte[] grabScreen() { 
		try {
                        //System.out.println("Dimension : " + Toolkit.getDefaultToolkit().getScreenSize());
			return ImgToByteArray(new Robot()
                                .createScreenCapture(new Rectangle(config.getOffsetX(), config.getOffsetY(), 
                                                                    config.getWidth(), config.getHeight())));
		} catch (SecurityException e) {
                    
		} catch (AWTException e) {
		}
		return null;
    }
  
    public byte[] grabScreen2() { 
		try {
                    ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
                    ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
                    jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    jpgWriteParam.setCompressionQuality(config.getCompressionRate());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imgOut = new MemoryCacheImageOutputStream(baos);
                    jpgWriter.setOutput(imgOut);
                    
                    BufferedImage img = new Robot()
                                .createScreenCapture(new Rectangle(config.getOffsetX(), config.getOffsetY(), 
                                                                    config.getWidth(), config.getHeight()));
                    
                    jpgWriter.write(null, new IIOImage(img, null, null), jpgWriteParam);
                        //System.out.println("Dimension : " + Toolkit.getDefaultToolkit().getScreenSize());
                    baos.flush();
                    byte[] buf = baos.toByteArray();
                    baos.close();
                        
                    return buf; 
		} catch (SecurityException e) {
                    
		} catch (AWTException e) {
		} catch (IOException ex) {
                }
		return null;
    }
    
    public BufferedImage convertRenderedImage(RenderedImage img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		ColorModel cm = img.getColorModel();
		int width = img.getWidth();
		int height = img.getHeight();
		WritableRaster raster = cm
				.createCompatibleWritableRaster(width, height);
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		Hashtable properties = new Hashtable();
		String[] keys = img.getPropertyNames();
		if (keys != null) {
			for (int i = 0; i < keys.length; i++) {
				properties.put(keys[i], img.getProperty(keys[i]));
			}
		}
		BufferedImage result = new BufferedImage(cm, raster,
				isAlphaPremultiplied, properties);
		img.copyData(raster);
		return result;
    }
    
    public static byte[] ImgToByteArray(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write( img, "png", baos );
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return imageInByte;
        } catch (IOException ex) {
            System.out.println("EXCEPTION IN ImgToByteArray: " + ex);
            return null;
        }
    }
    
    public static BufferedImage ImgFromByteArray(byte[] arr) {
        InputStream in = new ByteArrayInputStream(arr);
        try {
            return ImageIO.read(in);
        } catch (IOException ex) {
            System.out.println("EXCEPTION IN ImgFromByteArray: " + ex);
            return null;
        }
    }
}
