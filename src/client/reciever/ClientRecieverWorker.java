/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.reciever;

import java.awt.image.BufferedImage;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import screen.ScreenCapture;
import server.ClientHandler;
/**
 *
 * @author wookie
 */
public class ClientRecieverWorker extends SwingWorker<Integer, ImageIcon> {
    private DataInputStream in;
    private DataOutputStream out;
    private InputStream sin;
    private OutputStream sout;
    private javax.swing.JLabel label;
    private int filterCount;
    private int filterFrom;
    
    private static Logger logger = Logger.getLogger(ClientRecieverWorker.class.getName());
    private FileHandler fh;    

    public ClientRecieverWorker() {
        
    }
    
    public ClientRecieverWorker(JLabel label, int filterCount, int filterFrom) {
        this.label = label;
        this.filterCount = filterCount;
        this.filterFrom = filterFrom;
    }
    
    public boolean printArray(byte[] array) {
        StringBuilder strBuild = new StringBuilder();
        int zeroCount = 0;
        
        for(int i = 0; i < 15; i++) 
            strBuild.append(array[i]).append(" ");
            //System.out.print(array[i] + " ");
        
//        System.out.print(".....");
        strBuild.append(".....");
        
        for(int i = array.length - 1; i > array.length - 15; i--) {
//            System.out.print(array[i] + " ");
            strBuild.append(array[i]).append(" ");
            
            
        }
        
        for(int i = array.length - 1; i > array.length - filterFrom; i--) {
            if(array[i] == 0)
                zeroCount++;
            
            if(zeroCount > filterCount) {
                logger.log(Level.INFO, "AAA zerocount", zeroCount);
                return false;
            }
        }
        
//        System.out.println("");
        strBuild.append("");
        logger.info(strBuild.toString());
        return true;
    }
    
    @Override
    protected Integer doInBackground() throws Exception {
        fh = new FileHandler("ClientLog.log");  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter); 
        
        long start;
        long end;
        final int bufferMaxLength = 1024*(256);
        int bufferSize = 2048;
//        byte[] buffer = new byte[bufferSize];
        byte[] buffer = new byte[bufferMaxLength];
        int read;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        in = new DataInputStream(sin);
        out = new DataOutputStream(sout);
        
        int size = 0;
        int currentSize = 0;
        BufferedImage img = null;
        ImageIcon imgIcon = null;
        
//        int count = 0;
//        while (true) {
//            
//            //size = sin.read(buffer); 
//            //System.out.println("size: " + size);
//            
//            start = System.nanoTime();
//            while( ( size = sin.read( buffer ) ) > 0 ) {  
////                try {
//                    baos.write( buffer, 0, size ); 
//                    count++;
////                } catch (Exception ex) {
////                    System.out.println("BAD1 " + ex);
////                } catch (Error er) {
////                    System.out.println("VERY BAD1 " + er);
////                }
//                
////                try {
//                    currentSize += size;
////                } catch (Exception ex) {
////                    System.out.println("BAD2 " + ex);
////                } catch (Error er) {
////                    System.out.println("VERY BAD2 " + er);
////                }
//                //System.out.println("size : " + size);
//                if(size < bufferSize) {
//                    System.out.println("COUNT: " + count);
//                    System.out.println("SIZE: " + baos.toByteArray().length);
//                    count = 0;
//                    break;
//                }
//            }
//            end = System.nanoTime();
//            System.out.println("Read buf: " + (end-start));
//            //System.out.println("YEEE : " + baos.toByteArray().length);
//            //if(size <= bufferLength)
////            start = System.nanoTime();
//            if((currentSize > bufferMaxLength) || (currentSize <= 0)) {
//                System.out.println("BAD BAD");
//            }
//            else {
////                try {
//                img = ScreenCapture.ImgFromByteArray(baos.toByteArray());
////                } catch (Exception ex) {
////                    System.out.println("BAD3 " + ex);
////                } catch (Error er) {
////                    System.out.println("VERY BAD3 " + er);
////                }
////            end = System.nanoTime();
////            System.out.println("image from buf: " + (end-start));
//            
////            start = System.nanoTime();
////            end = System.nanoTime();
////            System.out.println("Reset baos: " + (end-start));
//            
//            //else
//            //    System.out.println("WAT? " + size + " :: " + bufferLength);
//           
//            //if(img != null)
////            start = System.nanoTime();
////                try {
//                    if(img != null) {
//                        imgIcon = new ImageIcon(img);
//                    } else {
//                        System.out.println("IMG IS NULL ?");
//                    }
////                } catch (Exception ex) {
////                    System.out.println("BAD4 " + ex);
////                } catch (Error er) {
////                    System.out.println("VERY BAD4 " + er);
////                }
////            end = System.nanoTime();
////            System.out.println("Image icon: " + (end-start));
//            
////            start = System.nanoTime();
//                publish(imgIcon);
//                System.out.println("Published");
//            }
//            
////            try {
//            baos.reset();
//            currentSize = 0;
////            } catch (Exception ex) {
////                    System.out.println("BAD5 " + ex);
////                } catch (Error er) {
////                    System.out.println("VERY BAD5 " + er);
////                }
//////            end = System.nanoTime();
////            System.out.println("Publish: " + (end-start));
//        }

//        int size1;
//        byte[] ar1 = new byte[20000];
//        while(true) {
//            size1 = in.readInt();
//            System.out.println("1: " + size1);
//            
//            if((size1 > 0) && (size1 <= 20000)) {
//                in.read(ar1, 0, size1);
//                System.out.println("Ye");
//            } else {
//                System.out.println("No");
//            }
//        }

//-----------------------------------------------------------------------------
//        ByteArrayOutputStream baos;
//        String str;
//        int step = 0;
//        int count = 0;
//        while (true) {
////            System.out.println("---------------------------------------------");
//            logger.info("---------------------------------------------");
//            step += 1;
//            baos = new ByteArrayOutputStream();
////            System.out.println("STEP: " + step);
//            logger.log(Level.INFO, "STEP: {0}", step);
//            
//            
//            try { 
//                count = in.readInt();
//                //out.write(0);
////                System.out.println("SIZE " + size);
//                logger.log(Level.INFO, "count {0}", count);
//                
//            } catch (Exception ex) {
////                System.out.println("WORSE " + ex);
//                logger.log(Level.INFO, "WORSE {0}", ex);
//            } catch (Error er) {
////                System.out.println("THE WORST" + er);
//                logger.log(Level.INFO, "THE WORST{0}", er);
//            }
//            
//            if(count > 0)
//            for(int i = 0; i < count + 1; i++) {
//                
//            try { 
//                size = in.readInt();
//                logger.log(Level.INFO, "SIZE {0}", size);
//            } catch (Exception ex) {
//                logger.log(Level.INFO, "WORSE {0}", ex);
//            } catch (Error er) {
//                logger.log(Level.INFO, "THE WORST{0}", er);
//            }
//
//                
//            try {
//                if((size < 0) || (size > bufferMaxLength)) {
//                    logger.log(Level.INFO, "I''m here {0} ::: {1}", new Object[]{size, bufferMaxLength});
//                } else {
//                    currentSize = size;
//                }
//            } catch (Exception ex) {
//                logger.log(Level.INFO, "WORSE1 {0}", ex);
//                logger.log(Level.INFO, "Current Size: {0}", currentSize);
//                logger.log(Level.INFO, "Size: {0}", size);
//            } catch (Error er) {
////                System.out.println("THE WORST1" + er);
//                logger.log(Level.INFO, "THE WORST1{0}", er);
////                System.out.println("Current Size: " + currentSize);
//                logger.log(Level.INFO, "Current Size: {0}", currentSize);
////                System.out.println("Size: " + size);
//                logger.log(Level.INFO, "Size: {0}", size);
//            }
//
//            
//            try {    
//                buffer = new byte[currentSize];
//                in.read(buffer, 0, currentSize);
//                baos.write(buffer);
//                //out.write(0);
////                System.out.println("BUFFER " + buffer);
//                logger.log(Level.INFO, "BUFFER {0}", buffer);
//                printArray(buffer);
//            } catch (Exception ex) {
////                System.out.println("WORSE2 " + ex);
//                logger.log(Level.INFO, "WORSE2 {0}", ex);
//            } catch (Error er) {
////                System.out.println("THE WORST2" + er);
//                logger.log(Level.INFO, "THE WORST2{0}", er);
//            }    
//
//            }
////                end = System.nanoTime();
////                System.out.println("Read array: " + (end-start));
////                while((read = sin.read(input)) != -1) {
////                    System.out.println("WATAFAK");
////                }
////                while( ( read = sin.read( input ) ) != -1 ) {  
////                     baos.write( input, 0, read );  
////                }
//                //System.out.println("Recieved: " + input[0]);
////                System.out.println("YESSS??");
//
////                start = System.nanoTime();
//            try {
//                img = ScreenCapture.ImgFromByteArray(baos.toByteArray());
////                System.out.println("INGGG: " + img);
//                logger.log(Level.INFO, "INGGG: {0}", img);
//            } catch (Exception ex) {
////                System.out.println("WORSE3 " + ex);
//                logger.log(Level.INFO, "WORSE3 {0}", ex);
//            } catch (Error er) {
////                System.out.println("THE WORST3" + er);
//                logger.log(Level.INFO, "THE WORST3{0}", er);
//            }    
////                end = System.nanoTime();
////                System.out.println("To IMAGE casting: " + (end-start));
//                
//            try {   
//                if(img != null) {
//                    imgIcon = new ImageIcon(img);
//                    publish(imgIcon);
//                    
////                    ImageIO.write(img, 
////                             "jpg",
////                             new File("img" + step + ".jpg"));
//                    
////                    System.out.println("PUBLISHED");
//                    logger.info("PUBLISHED");
//                } else {
////                    System.out.println("NOT PUBLISHED");
//                    logger.info("NOT PUBLISHED");
//                }
//            } catch (Exception ex) {
////                System.out.println("WORSE4 " + ex);
//                logger.log(Level.INFO, "WORSE4 {0}", ex);
////                System.out.println("Icon: " + imgIcon);
//                logger.log(Level.INFO, "Icon: {0}", imgIcon);
////                System.out.println("Image: " + img);
//                logger.log(Level.INFO, "Image: {0}", img);
////                System.out.println("Size: " + currentSize);
//                logger.log(Level.INFO, "Size: {0}", currentSize);
//            } catch (Error er) {
////                System.out.println("THE WORST4" + er);
//                logger.log(Level.INFO, "THE WORST4{0}", er);
//            }    
////            } catch (IOException ex) {
////                System.out.println("BAD");
////            } catch (Exception ex) {
////                System.out.println("WORSE " + ex);
////            } catch (Error er) {
////                System.out.println("THE WORST" + er);
////            }
//        }

//-----------------------------------------------------------------------------

        String str;
        int step = 0;
        while (true) {
//            System.out.println("---------------------------------------------");
            logger.info("---------------------------------------------");
            step += 1;
//            System.out.println("STEP: " + step);
            logger.log(Level.INFO, "STEP: {0}", step);
            //try {
            
//                System.out.println("HERE!");
                
                start = System.nanoTime();
            try { 
                size = in.readInt();
                //out.write(0);
//                System.out.println("SIZE " + size);
                logger.log(Level.INFO, "SIZE {0}", size);
                
            } catch (Exception ex) {
//                System.out.println("WORSE " + ex);
                logger.log(Level.INFO, "WORSE {0}", ex);
            } catch (Error er) {
//                System.out.println("THE WORST" + er);
                logger.log(Level.INFO, "THE WORST{0}", er);
            }
                end = System.nanoTime();
//                System.out.println("Read int time: " + (end-start));
                logger.log(Level.INFO, "Read int time: {0}", (end-start));
                
//                start = System.nanoTime();
            try {
                if((size < 0) || (size > bufferMaxLength)) {
//                    System.out.println("I'm here " + size + " ::: " + bufferMaxLength);
                    logger.log(Level.INFO, "I''m here {0} ::: {1}", new Object[]{size, bufferMaxLength});
                } else {
                    currentSize = size;
                }
                //input = new byte[currentSize];
            } catch (Exception ex) {
//                System.out.println("WORSE1 " + ex);
                logger.log(Level.INFO, "WORSE1 {0}", ex);
//                System.out.println("Current Size: " + currentSize);
                logger.log(Level.INFO, "Current Size: {0}", currentSize);
//                System.out.println("Size: " + size);
                logger.log(Level.INFO, "Size: {0}", size);
            } catch (Error er) {
//                System.out.println("THE WORST1" + er);
                logger.log(Level.INFO, "THE WORST1{0}", er);
//                System.out.println("Current Size: " + currentSize);
                logger.log(Level.INFO, "Current Size: {0}", currentSize);
//                System.out.println("Size: " + size);
                logger.log(Level.INFO, "Size: {0}", size);
            }
//                System.out.println("RES = " + size);
                //System.out.println(in.readUTF());
                start = System.nanoTime();
            try {    
                buffer = new byte[currentSize];
                in.read(buffer, 0, currentSize);
                //out.write(0);
//                System.out.println("BUFFER " + buffer);
                logger.log(Level.INFO, "BUFFER {0}", buffer);
            } catch (Exception ex) {
//                System.out.println("WORSE2 " + ex);
                logger.log(Level.INFO, "WORSE2 {0}", ex);
            } catch (Error er) {
//                System.out.println("THE WORST2" + er);
                logger.log(Level.INFO, "THE WORST2{0}", er);
            }    
                end = System.nanoTime();
//                System.out.println("Read byte size: " + (end-start));
                logger.log(Level.INFO, "Read byte size: {0}", (end-start));
//                end = System.nanoTime();
//                System.out.println("Read array: " + (end-start));
//                while((read = sin.read(input)) != -1) {
//                    System.out.println("WATAFAK");
//                }
//                while( ( read = sin.read( input ) ) != -1 ) {  
//                     baos.write( input, 0, read );  
//                }
                //System.out.println("Recieved: " + input[0]);
//                System.out.println("YESSS??");

//                start = System.nanoTime();
            try {
                if(printArray(buffer))
                    img = ScreenCapture.ImgFromByteArray(buffer);
//                System.out.println("INGGG: " + img);
                logger.log(Level.INFO, "INGGG: {0}", img);
            } catch (Exception ex) {
//                System.out.println("WORSE3 " + ex);
                logger.log(Level.INFO, "WORSE3 {0}", ex);
            } catch (Error er) {
//                System.out.println("THE WORST3" + er);
                logger.log(Level.INFO, "THE WORST3{0}", er);
            }    
//                end = System.nanoTime();
//                System.out.println("To IMAGE casting: " + (end-start));
                
            try {   
                if(img != null) {
                    imgIcon = new ImageIcon(img);
                    publish(imgIcon);
                    
//                    ImageIO.write(img, 
//                             "jpg",
//                             new File("img" + step + ".jpg"));
                    
//                    System.out.println("PUBLISHED");
                    logger.info("PUBLISHED");
                } else {
//                    System.out.println("NOT PUBLISHED");
                    logger.info("NOT PUBLISHED");
                }
            } catch (Exception ex) {
//                System.out.println("WORSE4 " + ex);
                logger.log(Level.INFO, "WORSE4 {0}", ex);
//                System.out.println("Icon: " + imgIcon);
                logger.log(Level.INFO, "Icon: {0}", imgIcon);
//                System.out.println("Image: " + img);
                logger.log(Level.INFO, "Image: {0}", img);
//                System.out.println("Size: " + currentSize);
                logger.log(Level.INFO, "Size: {0}", currentSize);
            } catch (Error er) {
//                System.out.println("THE WORST4" + er);
                logger.log(Level.INFO, "THE WORST4{0}", er);
            }    
//            } catch (IOException ex) {
//                System.out.println("BAD");
//            } catch (Exception ex) {
//                System.out.println("WORSE " + ex);
//            } catch (Error er) {
//                System.out.println("THE WORST" + er);
//            }
        }
       

//for(int i = 0; i < 150; i++) {
//    try {
//         BufferedImage img = ImageIO.read(new File("screen0.png"));
//         ImageIcon icon = new ImageIcon(img);
//         publish(icon);
//      } catch (IOException e) {
//         System.out.println("bad!");
//      }
//}

        //System.out.println("RETURN ????");
//        return null;
    }
    
    @Override
    protected void process(final List<ImageIcon> chunks) {
        long start;
        long end;
                
        //System.out.println("PROCESS");
        for (final ImageIcon res : chunks) {
//            start = System.nanoTime();
            label.setIcon(res);
            
//            end = System.nanoTime();
//            System.out.println("Ing show: " + (end-start));
        }
  }
    
    
//    public static void main(String[] ar) {
//        int serverPort = 6666; // здесь обязательно нужно указать порт к которому привязывается сервер.
//        String address = "127.0.0.1"; // это IP-адрес компьютера, где исполняется наша серверная программа. 
//                                      // Здесь указан адрес того самого компьютера где будет исполняться и клиент.
//
//
//
//            while (true) {
//            try {
//                System.out.println(in.readUTF());
//            } catch (IOException ex) {
//                Logger.getLogger(ClientReciever.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            }
////            // Создаем поток для чтения с клавиатуры.
////            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
////            String line = null;
////            System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
////            System.out.println();
////
////            while (true) {
////                line = keyboard.readLine(); // ждем пока пользователь введет что-то и нажмет кнопку Enter.
////                System.out.println("Sending this line to the server...");
////                out.writeUTF(line); // отсылаем введенную строку текста серверу.
////                out.flush(); // заставляем поток закончить передачу данных.
////                line = in.readUTF(); // ждем пока сервер отошлет строку текста.
////                System.out.println("The server was very polite. It sent me this : " + line);
////                System.out.println("Looks like the server is pleased with us. Go ahead and enter more lines.");
////                System.out.println();
////            }
//
//    }

    @Override
    protected void done() {
        System.out.println("DONE &!&!&!&!");
        this.execute();
    }

    public void setSin(InputStream sin) {
        this.sin = sin;
    }

    public void setSout(OutputStream sout) {
        this.sout = sout;
    }
   
}
