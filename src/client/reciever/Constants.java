/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.reciever;

/**
 *
 * @author wookie
 */
public interface Constants {
    String MOUSE_MOVE_COMMAND = "MOVE ";
    String MOUSE_DRAG_COMMAND = "DRAG ";
    String MOUSE_PRESS_COMMAND = "PRESS ";
    String MOUSE_RELEASE_COMMAND = "RELEASE ";
    String KEY_PRESS_COMMAND = "KEYPRESSED ";
    String KEY_RELEASE_COMMAND = "KEYRELEASE ";
}
