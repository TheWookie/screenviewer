/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.reciever;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author wookie
 */
public class ClientInit {
    public Socket establishConnection(String ip) {
        int serverPort = 6666; // здесь обязательно нужно указать порт к которому привязывается сервер.
        String address = ip; // это IP-адрес компьютера, где исполняется наша серверная программа. 
                                      // Здесь указан адрес того самого компьютера где будет исполняться и клиент.
                                      
         try {
            InetAddress ipAddress = InetAddress.getByName(address); // создаем объект который отображает вышеописанный IP-адрес.
            System.out.println("Any of you heard of a socket with IP address " + address + " and port " + serverPort + "?");
            Socket socket = new Socket(ipAddress, serverPort); // создаем сокет используя IP-адрес и порт сервера.
            System.out.println("Yes! I just got hold of the program.");

//            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом. 
//            sin = socket.getInputStream();
//            sout = socket.getOutputStream();
//
//            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
//            in = new DataInputStream(sin);
//            out = new DataOutputStream(sout);
            return socket;
//            out.writeInt(mode);
//            out.flush();
        } catch (Exception x) {
            System.out.println("Exception in reciever client!");
            return null;
        }       
    }
}
