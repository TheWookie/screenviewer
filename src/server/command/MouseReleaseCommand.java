/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.command;

/**
 *
 * @author wookie
 */
public class MouseReleaseCommand extends Command {

    @Override
    public void process(String... params) throws IllegalArgumentException {
        robot.mouseRelease(mouseButtons.get(params[1]));
    }
    
}
