/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.command;

import client.reciever.Constants;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author wookie
 */
public final class CommandList {
    private Map<String, Command> list = new HashMap<>();
    
    {
        list.put(Constants.MOUSE_MOVE_COMMAND.trim(), new MouseMoveCommand());
        list.put(Constants.MOUSE_DRAG_COMMAND.trim(), new MouseDragCommand());
        list.put(Constants.MOUSE_PRESS_COMMAND.trim(), new MousePressCommand());
        list.put(Constants.MOUSE_RELEASE_COMMAND.trim(), new MouseReleaseCommand());
        list.put(Constants.KEY_PRESS_COMMAND.trim(), new KeyPressCommand());
        list.put(Constants.KEY_RELEASE_COMMAND.trim(), new KeyReleaseCommand());
    }

    public Map<String, Command> getList() {
        return list;
    }
    
}
