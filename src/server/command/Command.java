/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.command;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import screen.ScreenInit;

/**
 *
 * @author wookie
 */
public abstract class Command {
    protected Robot robot;
    protected ScreenInit screenInit = ScreenInit.getInstance();
    protected final Map<String, Integer> mouseButtons = new HashMap<>();
    {
        mouseButtons.put("1", InputEvent.BUTTON1_MASK);
        mouseButtons.put("2", InputEvent.BUTTON2_MASK);
        mouseButtons.put("3", InputEvent.BUTTON3_MASK);
    }
    
    public Command() {
        try {
            robot = new Robot();
        } catch (AWTException ex) {
        }
    }
    
    /**
     * 
     * @param offsetX 
     * @param offsetY
     * @param params First argument is always name of the command. 
     * All other arguments are parameters of the command.
     */
    public abstract void process(String... params) throws IllegalArgumentException;
}
