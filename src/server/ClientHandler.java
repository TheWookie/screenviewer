/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.reciever.ClientRecieverWorker;
import client.reciever.Constants;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.JLabel;
import screen.ScreenCapture;
import screen.ScreenInit;

/**
 *
 * @author wookie
 */
public class ClientHandler extends Thread {
    private final Socket socket;
    private InputStream sin;
    private OutputStream sout;
    private ScreenCapture capture;
    
    private static Logger logger = Logger.getLogger(ClientHandler.class.getName());
    private FileHandler fh;    

    public ClientHandler(Socket socket) {
        System.out.println("ClientHandler is ready");
        this.socket = socket;
        this.capture = new ScreenCapture();
    }
    
    @Override
    public void run() {
        try {  
            fh = new FileHandler("ServerLog.log");
        } catch (IOException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter); 
        
        System.out.println("Got a client");
        System.out.println();

        // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиенту. 
        try {
            sin = socket.getInputStream();
            sout = socket.getOutputStream();

        
        // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
        DataInputStream in = new DataInputStream(sin);
        DataOutputStream out = new DataOutputStream(sout);


        System.out.println("Start sending");
        
        long start;
        long end;
        //byte[] data;
        
//        while (true) {
//            BufferedImage img = capture.grabScreen();
//            System.out.println("Array length " + ScreenCapture.ImgToByteArray(img).length);
//            //data = ScreenCapture.ImgToByteArray(img);
//            out.write(ScreenCapture.ImgToByteArray(img));
//            //sout.flush();
//        }
        
        int step = 0;
        int cSize = 512;
        byte[] data;
        byte[] smallData = new byte[cSize];
        int smallDataCount;
        while(true) {
            start = System.nanoTime();
        //while(step < Integer.MAX_VALUE) {
        //for(int i = 0; i < 10 ; i++) {
            step += 1;
        
//            System.out.println("-----------------------------------------");
            logger.info("-----------------------------------------");
//            System.out.println("STEP: " + step);
            logger.log(Level.INFO, "STEP: {0}", step);
//            BufferedImage img = capture.grabScreen();
            data = capture.grabScreen2();
            
            //System.out.println("IMAGE: " + img);
   
            //sout.write(ScreenCapture.ImgToByteArray(img));
//            start = System.nanoTime();
//            data = ScreenCapture.ImgToByteArray(img);
            //smallData = new byte[cSize];
            //smallDataCount = (int)data.length/cSize;
            //out.writeInt(smallDataCount);
            
//            for(int i = 0; i < smallDataCount; i++) {
//            
//                System.arraycopy( data, i*cSize, smallData, 0, smallData.length );
////            end = System.nanoTime();
////            System.out.println("To byte casting: " + (end-start));
////            System.out.println("Array length " + data.length);
//            
//
////            start = System.nanoTime();
//                out.writeInt(smallData.length);
////            end = System.nanoTime();
////            System.out.println("INT SEND: " + (end-start));
//            //sin.read();
////            out.writeUTF(Integer.toString(data.length));
//            //out.flush();
//            
////            start = System.nanoTime();
//                out.write(smallData);
////            end = System.nanoTime();
////            System.out.println("INT SEND: " + (end-start));
//            //in.read();
//            //sout.flush();
//            
//                printArray(smallData);
//            }
            //System.arraycopy(data, smallDataCount*cSize, smallData, 0, data.length-smallDataCount*cSize);
            out.writeInt(data.length);
            out.write(data);
            printArray(data);
            
            end = System.nanoTime();
            
            logger.log(Level.INFO, "Array length {0}", data.length);
            logger.log(Level.INFO, "INT SEND:", (end-start));
            
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(ClientHandler.class.getName()).log(Level.SEVERE, null, ex);
//                }
        }
//
//        BufferedImage img;
//        byte[] data = new byte[100];
//        int size = 100;
//       
//        
//        while(true) {
//            img = capture.grabScreen();
//            data = ScreenCapture.ImgToByteArray(img);
//            size = data.length;
//            System.out.println(size);
//            
//            out.writeInt(size);
//            
//            out.write(data);
//            
////            img = capture.grabScreen();
////            size = ScreenCapture.ImgToByteArray(img).length;
////            System.out.println(size);
////            out.writeInt(size);
//        }
//        
//        //System.out.println("Stop sending");
//            
//           
        } catch (IOException ex) {
//            System.out.println("EXCEPTION IO " + ex);
            logger.log(Level.INFO, "EXCEPTION IO {0}", ex);
//        } catch (Exception ex) {
//            System.out.println("EXCEPTION Just " + ex);
        } catch (Error er) {
//            System.out.println("ERRRRRORRRR " + er);
            logger.log(Level.INFO, "ERRRRRORRRR {0}", er);
        }
//        
//        
////        String line = null;
////        try {
////            while(true) {
////                line = in.readUTF(); // ожидаем пока клиент пришлет строку текста.
////                System.out.println("The dumb client just sent me this line : " + line);
////                System.out.println("I'm sending it back...");
////                out.writeUTF(line); // отсылаем клиенту обратно ту самую строку текста.
////                out.flush(); // заставляем поток закончить передачу данных.
////                System.out.println("Waiting for the next line...");
////                System.out.println();
////            }
////        } catch (IOException ex) {
////            
////        }
//
    }
    
    public static void printArray(byte[] array) {
        StringBuilder strBuild = new StringBuilder();
        
        for(int i = 0; i < 15; i++) 
            strBuild.append(array[i]).append(" ");
            //System.out.print(array[i] + " ");
        
//        System.out.print(".....");
        strBuild.append(".....");
        
        for(int i = array.length - 1; i > array.length - 15; i--) 
//            System.out.print(array[i] + " ");
            strBuild.append(array[i]).append(" ");
        
//        System.out.println("");
        strBuild.append("");
        logger.info(strBuild.toString());
        
    }
}
