/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import screen.ScreenInit;
import server.command.Command;
import server.command.CommandList;

public class CommandHandler extends Thread {
    private InputStream sin;
    private CommandList commandList = new CommandList();
    private Command currentCommand;

    public CommandHandler(InputStream sin) {
        this.sin = sin;
    }

    private String[] fetchCommand(String command) {
        return command.split("\\s+");
    }
    
    @Override
    public void run() {
        DataInputStream in = new DataInputStream(sin);
        String line;
        String[] splittedLine;
        
        try {
            while(true) {
                line = in.readUTF();
                splittedLine = fetchCommand(line);

                currentCommand = commandList.getList().get(splittedLine[0]);
                currentCommand.process(splittedLine);
            }
        } catch (IOException ex) {
            System.out.println("Command exception: " + ex);
        }
    }
    
  public static void main(String[] args) throws Exception{

    Robot r = new Robot();
    r.mouseMove(35,35);
    r.mousePress( InputEvent.BUTTON1_MASK );
    r.mouseRelease( InputEvent.BUTTON1_MASK );
    Thread.sleep(50);
    r.mousePress( InputEvent.BUTTON1_MASK );
    r.mouseRelease( InputEvent.BUTTON1_MASK );
  }
}