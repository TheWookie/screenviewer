/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.net.*;
import java.io.*;
import screen.ScreenInit;

/**
 * kafka, zookeeper, rabbitmq
 * @author wookie
 */
public class Server {
    public static void main(String[] ar)    {
        int port = 6666; // случайный порт (может быть любое число от 1025 до 65535)
        ScreenInit screenInit = ScreenInit.getInstance();
        
        screenInit.setMaxWidth(1366);
        screenInit.setNaxHeight(768);
        screenInit.graphicConfigInit();
        try {
            ServerSocket ss = new ServerSocket(port); // создаем сокет сервера и привязываем его к вышеуказанному порту
            while(true) {
                System.out.println("Waiting for a client...");
                Socket socket = ss.accept(); // заставляем сервер ждать подключений и выводим сообщение когда кто-то связался с сервером
            
                InputStream sin = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();
                
                ClientHandler clientHandler = new ClientHandler(socket);
                clientHandler.start();
                
                CommandHandler commandHandler = new CommandHandler(sin);
                commandHandler.start();
            }
        } catch (IOException ex) {
            System.out.println("Exception! " + ex);
        }
   }
}
